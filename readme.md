# Practice gitlab ci

#### 1. Go to fork the project 

To know how to fork go [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) 

#### 2. Clone the repo forked :

```bash
git clone ${YOU_REMOTE_URL}
```

#### 3. Fill the `gitlab-ci.yml`

1. Set the environnement image :

```yaml
image: python:3.7
```

2. Set your stages, here we will have only test and package stages

```yaml
stages:
  - test
  - package
```

3. Write the job test

```yaml
job_test_kata:
  stage: test
  script: 
    - pip install pytest # Install pytest dependencies to test your python code
    - pytest FizzBuzzKata/test --junitxml=report.xml
  artifacts:
    when: always
    reports:
      junit: report.xml
```

4. Write the job package

```yaml
job_package_kata:
  stage: package
  script: mv FizzBuzzKata/src/FizzBuzz.py .
  artifacts: # Permit to make an artifact attached to your pipeline
    paths:
      - FizzBuzz.py
    expire_in: 1 week
```

#### 4. Make a changes Into the `readme.md` and push it.
CHANGE
#### 5. Go to the gitlabci section 

![gitlab_ci_section](docs/img/gitlabci_section.png) 

### 6. You can download the code artefact

![download_artifact_code](docs/img/download_artifact_code.png)

### 7. Go to see the gitlab test report

`CI/CD > Pipelines > Select a pipeline status > Tests`

![tests_display](docs/img/tests_display.png)

